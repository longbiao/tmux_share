from collections import Iterable
def fib(max):
    n, a, b = 0, 0, 1
    while n < max:
        yield b
        a, b = b, a + b
        n = n + 1
    return 'done'

print(isinstance(fib(3),Iterable))
print(type(fib))
for i in fib(3):
    print(i)
