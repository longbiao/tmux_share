
bench:     file format elf64-x86-64


Disassembly of section .init:

0000000000400670 <_init>:
  400670:	48 83 ec 08          	sub    $0x8,%rsp
  400674:	48 8b 05 7d 19 20 00 	mov    0x20197d(%rip),%rax        # 601ff8 <_DYNAMIC+0x1e0>
  40067b:	48 85 c0             	test   %rax,%rax
  40067e:	74 05                	je     400685 <_init+0x15>
  400680:	e8 4b 00 00 00       	callq  4006d0 <__gmon_start__@plt>
  400685:	48 83 c4 08          	add    $0x8,%rsp
  400689:	c3                   	retq   

Disassembly of section .plt:

0000000000400690 <srandom@plt-0x10>:
  400690:	ff 35 72 19 20 00    	pushq  0x201972(%rip)        # 602008 <_GLOBAL_OFFSET_TABLE_+0x8>
  400696:	ff 25 74 19 20 00    	jmpq   *0x201974(%rip)        # 602010 <_GLOBAL_OFFSET_TABLE_+0x10>
  40069c:	0f 1f 40 00          	nopl   0x0(%rax)

00000000004006a0 <srandom@plt>:
  4006a0:	ff 25 72 19 20 00    	jmpq   *0x201972(%rip)        # 602018 <_GLOBAL_OFFSET_TABLE_+0x18>
  4006a6:	68 00 00 00 00       	pushq  $0x0
  4006ab:	e9 e0 ff ff ff       	jmpq   400690 <_init+0x20>

00000000004006b0 <printf@plt>:
  4006b0:	ff 25 6a 19 20 00    	jmpq   *0x20196a(%rip)        # 602020 <_GLOBAL_OFFSET_TABLE_+0x20>
  4006b6:	68 01 00 00 00       	pushq  $0x1
  4006bb:	e9 d0 ff ff ff       	jmpq   400690 <_init+0x20>

00000000004006c0 <__libc_start_main@plt>:
  4006c0:	ff 25 62 19 20 00    	jmpq   *0x201962(%rip)        # 602028 <_GLOBAL_OFFSET_TABLE_+0x28>
  4006c6:	68 02 00 00 00       	pushq  $0x2
  4006cb:	e9 c0 ff ff ff       	jmpq   400690 <_init+0x20>

00000000004006d0 <__gmon_start__@plt>:
  4006d0:	ff 25 5a 19 20 00    	jmpq   *0x20195a(%rip)        # 602030 <_GLOBAL_OFFSET_TABLE_+0x30>
  4006d6:	68 03 00 00 00       	pushq  $0x3
  4006db:	e9 b0 ff ff ff       	jmpq   400690 <_init+0x20>

00000000004006e0 <time@plt>:
  4006e0:	ff 25 52 19 20 00    	jmpq   *0x201952(%rip)        # 602038 <_GLOBAL_OFFSET_TABLE_+0x38>
  4006e6:	68 04 00 00 00       	pushq  $0x4
  4006eb:	e9 a0 ff ff ff       	jmpq   400690 <_init+0x20>

00000000004006f0 <random@plt>:
  4006f0:	ff 25 4a 19 20 00    	jmpq   *0x20194a(%rip)        # 602040 <_GLOBAL_OFFSET_TABLE_+0x40>
  4006f6:	68 05 00 00 00       	pushq  $0x5
  4006fb:	e9 90 ff ff ff       	jmpq   400690 <_init+0x20>

0000000000400700 <malloc@plt>:
  400700:	ff 25 42 19 20 00    	jmpq   *0x201942(%rip)        # 602048 <_GLOBAL_OFFSET_TABLE_+0x48>
  400706:	68 06 00 00 00       	pushq  $0x6
  40070b:	e9 80 ff ff ff       	jmpq   400690 <_init+0x20>

0000000000400710 <Sleef_currentTimeMicros@plt>:
  400710:	ff 25 3a 19 20 00    	jmpq   *0x20193a(%rip)        # 602050 <_GLOBAL_OFFSET_TABLE_+0x50>
  400716:	68 07 00 00 00       	pushq  $0x7
  40071b:	e9 70 ff ff ff       	jmpq   400690 <_init+0x20>

Disassembly of section .text:

0000000000400720 <_start>:
  400720:	31 ed                	xor    %ebp,%ebp
  400722:	49 89 d1             	mov    %rdx,%r9
  400725:	5e                   	pop    %rsi
  400726:	48 89 e2             	mov    %rsp,%rdx
  400729:	48 83 e4 f0          	and    $0xfffffffffffffff0,%rsp
  40072d:	50                   	push   %rax
  40072e:	54                   	push   %rsp
  40072f:	49 c7 c0 00 10 40 00 	mov    $0x401000,%r8
  400736:	48 c7 c1 90 0f 40 00 	mov    $0x400f90,%rcx
  40073d:	48 c7 c7 c4 08 40 00 	mov    $0x4008c4,%rdi
  400744:	e8 77 ff ff ff       	callq  4006c0 <__libc_start_main@plt>
  400749:	f4                   	hlt    
  40074a:	66 90                	xchg   %ax,%ax
  40074c:	0f 1f 40 00          	nopl   0x0(%rax)

0000000000400750 <deregister_tm_clones>:
  400750:	b8 67 20 60 00       	mov    $0x602067,%eax
  400755:	55                   	push   %rbp
  400756:	48 2d 60 20 60 00    	sub    $0x602060,%rax
  40075c:	48 83 f8 0e          	cmp    $0xe,%rax
  400760:	48 89 e5             	mov    %rsp,%rbp
  400763:	77 02                	ja     400767 <deregister_tm_clones+0x17>
  400765:	5d                   	pop    %rbp
  400766:	c3                   	retq   
  400767:	b8 00 00 00 00       	mov    $0x0,%eax
  40076c:	48 85 c0             	test   %rax,%rax
  40076f:	74 f4                	je     400765 <deregister_tm_clones+0x15>
  400771:	5d                   	pop    %rbp
  400772:	bf 60 20 60 00       	mov    $0x602060,%edi
  400777:	ff e0                	jmpq   *%rax
  400779:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)

0000000000400780 <register_tm_clones>:
  400780:	b8 60 20 60 00       	mov    $0x602060,%eax
  400785:	55                   	push   %rbp
  400786:	48 2d 60 20 60 00    	sub    $0x602060,%rax
  40078c:	48 c1 f8 03          	sar    $0x3,%rax
  400790:	48 89 e5             	mov    %rsp,%rbp
  400793:	48 89 c2             	mov    %rax,%rdx
  400796:	48 c1 ea 3f          	shr    $0x3f,%rdx
  40079a:	48 01 d0             	add    %rdx,%rax
  40079d:	48 d1 f8             	sar    %rax
  4007a0:	75 02                	jne    4007a4 <register_tm_clones+0x24>
  4007a2:	5d                   	pop    %rbp
  4007a3:	c3                   	retq   
  4007a4:	ba 00 00 00 00       	mov    $0x0,%edx
  4007a9:	48 85 d2             	test   %rdx,%rdx
  4007ac:	74 f4                	je     4007a2 <register_tm_clones+0x22>
  4007ae:	5d                   	pop    %rbp
  4007af:	48 89 c6             	mov    %rax,%rsi
  4007b2:	bf 60 20 60 00       	mov    $0x602060,%edi
  4007b7:	ff e2                	jmpq   *%rdx
  4007b9:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)

00000000004007c0 <__do_global_dtors_aux>:
  4007c0:	80 3d 99 18 20 00 00 	cmpb   $0x0,0x201899(%rip)        # 602060 <__TMC_END__>
  4007c7:	75 11                	jne    4007da <__do_global_dtors_aux+0x1a>
  4007c9:	55                   	push   %rbp
  4007ca:	48 89 e5             	mov    %rsp,%rbp
  4007cd:	e8 7e ff ff ff       	callq  400750 <deregister_tm_clones>
  4007d2:	5d                   	pop    %rbp
  4007d3:	c6 05 86 18 20 00 01 	movb   $0x1,0x201886(%rip)        # 602060 <__TMC_END__>
  4007da:	f3 c3                	repz retq 
  4007dc:	0f 1f 40 00          	nopl   0x0(%rax)

00000000004007e0 <frame_dummy>:
  4007e0:	48 83 3d 28 16 20 00 	cmpq   $0x0,0x201628(%rip)        # 601e10 <__JCR_END__>
  4007e7:	00 
  4007e8:	74 1e                	je     400808 <frame_dummy+0x28>
  4007ea:	b8 00 00 00 00       	mov    $0x0,%eax
  4007ef:	48 85 c0             	test   %rax,%rax
  4007f2:	74 14                	je     400808 <frame_dummy+0x28>
  4007f4:	55                   	push   %rbp
  4007f5:	bf 10 1e 60 00       	mov    $0x601e10,%edi
  4007fa:	48 89 e5             	mov    %rsp,%rbp
  4007fd:	ff d0                	callq  *%rax
  4007ff:	5d                   	pop    %rbp
  400800:	e9 7b ff ff ff       	jmpq   400780 <register_tm_clones>
  400805:	0f 1f 00             	nopl   (%rax)
  400808:	e9 73 ff ff ff       	jmpq   400780 <register_tm_clones>
  40080d:	0f 1f 00             	nopl   (%rax)

0000000000400810 <fill_double>:
  400810:	55                   	push   %rbp
  400811:	48 89 e5             	mov    %rsp,%rbp
  400814:	48 83 ec 40          	sub    $0x40,%rsp
  400818:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  40081c:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  40081f:	f2 0f 11 45 d8       	movsd  %xmm0,-0x28(%rbp)
  400824:	f2 0f 11 4d d0       	movsd  %xmm1,-0x30(%rbp)
  400829:	bf 00 00 00 00       	mov    $0x0,%edi
  40082e:	b8 00 00 00 00       	mov    $0x0,%eax
  400833:	e8 a8 fe ff ff       	callq  4006e0 <time@plt>
  400838:	89 c7                	mov    %eax,%edi
  40083a:	e8 61 fe ff ff       	callq  4006a0 <srandom@plt>
  40083f:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  400846:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  40084d:	eb 6b                	jmp    4008ba <fill_double+0xaa>
  40084f:	e8 9c fe ff ff       	callq  4006f0 <random@plt>
  400854:	f2 48 0f 2a d0       	cvtsi2sd %rax,%xmm2
  400859:	f2 0f 11 55 c8       	movsd  %xmm2,-0x38(%rbp)
  40085e:	e8 8d fe ff ff       	callq  4006f0 <random@plt>
  400863:	f2 48 0f 2a c0       	cvtsi2sd %rax,%xmm0
  400868:	f2 0f 10 0d 60 08 00 	movsd  0x860(%rip),%xmm1        # 4010d0 <__dso_handle+0xb8>
  40086f:	00 
  400870:	f2 0f 59 c1          	mulsd  %xmm1,%xmm0
  400874:	f2 0f 58 45 c8       	addsd  -0x38(%rbp),%xmm0
  400879:	f2 0f 10 0d 57 08 00 	movsd  0x857(%rip),%xmm1        # 4010d8 <__dso_handle+0xc0>
  400880:	00 
  400881:	f2 0f 5e c1          	divsd  %xmm1,%xmm0
  400885:	f2 0f 11 45 f0       	movsd  %xmm0,-0x10(%rbp)
  40088a:	8b 45 fc             	mov    -0x4(%rbp),%eax
  40088d:	48 98                	cltq   
  40088f:	48 8d 14 c5 00 00 00 	lea    0x0(,%rax,8),%rdx
  400896:	00 
  400897:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
  40089b:	48 01 d0             	add    %rdx,%rax
  40089e:	f2 0f 10 45 d0       	movsd  -0x30(%rbp),%xmm0
  4008a3:	f2 0f 5c 45 d8       	subsd  -0x28(%rbp),%xmm0
  4008a8:	f2 0f 59 45 f0       	mulsd  -0x10(%rbp),%xmm0
  4008ad:	f2 0f 58 45 d8       	addsd  -0x28(%rbp),%xmm0
  4008b2:	f2 0f 11 00          	movsd  %xmm0,(%rax)
  4008b6:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
  4008ba:	8b 45 fc             	mov    -0x4(%rbp),%eax
  4008bd:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
  4008c0:	7c 8d                	jl     40084f <fill_double+0x3f>
  4008c2:	c9                   	leaveq 
  4008c3:	c3                   	retq   

00000000004008c4 <main>:
  4008c4:	55                   	push   %rbp
  4008c5:	48 89 e5             	mov    %rsp,%rbp
  4008c8:	48 81 ec 90 00 00 00 	sub    $0x90,%rsp
  4008cf:	bf 00 08 af 2f       	mov    $0x2faf0800,%edi
  4008d4:	e8 27 fe ff ff       	callq  400700 <malloc@plt>
  4008d9:	48 89 45 e8          	mov    %rax,-0x18(%rbp)
  4008dd:	48 b8 00 00 00 00 00 	movabs $0x4000000000000000,%rax
  4008e4:	00 00 40 
  4008e7:	48 8b 55 e8          	mov    -0x18(%rbp),%rdx
  4008eb:	48 89 85 78 ff ff ff 	mov    %rax,-0x88(%rbp)
  4008f2:	f2 0f 10 8d 78 ff ff 	movsd  -0x88(%rbp),%xmm1
  4008f9:	ff 
  4008fa:	f2 0f 10 05 de 07 00 	movsd  0x7de(%rip),%xmm0        # 4010e0 <__dso_handle+0xc8>
  400901:	00 
  400902:	be 00 e1 f5 05       	mov    $0x5f5e100,%esi
  400907:	48 89 d7             	mov    %rdx,%rdi
  40090a:	e8 01 ff ff ff       	callq  400810 <fill_double>
  40090f:	b8 00 00 00 00       	mov    $0x0,%eax
  400914:	e8 f7 fd ff ff       	callq  400710 <Sleef_currentTimeMicros@plt>
  400919:	48 89 45 98          	mov    %rax,-0x68(%rbp)
  40091d:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  400924:	eb 32                	jmp    400958 <main+0x94>
  400926:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
  40092a:	48 8b 00             	mov    (%rax),%rax
  40092d:	48 89 85 78 ff ff ff 	mov    %rax,-0x88(%rbp)
  400934:	f2 0f 10 85 78 ff ff 	movsd  -0x88(%rbp),%xmm0
  40093b:	ff 
  40093c:	e8 cf 03 00 00       	callq  400d10 <func1>
  400941:	f2 0f 10 4d f0       	movsd  -0x10(%rbp),%xmm1
  400946:	f2 0f 58 c1          	addsd  %xmm1,%xmm0
  40094a:	f2 0f 11 45 f0       	movsd  %xmm0,-0x10(%rbp)
  40094f:	48 83 45 e8 08       	addq   $0x8,-0x18(%rbp)
  400954:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
  400958:	81 7d fc ff e0 f5 05 	cmpl   $0x5f5e0ff,-0x4(%rbp)
  40095f:	7e c5                	jle    400926 <main+0x62>
  400961:	b8 00 00 00 00       	mov    $0x0,%eax
  400966:	e8 a5 fd ff ff       	callq  400710 <Sleef_currentTimeMicros@plt>
  40096b:	48 2b 45 98          	sub    -0x68(%rbp),%rax
  40096f:	48 89 45 98          	mov    %rax,-0x68(%rbp)
  400973:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
  400977:	48 89 85 78 ff ff ff 	mov    %rax,-0x88(%rbp)
  40097e:	f2 0f 10 85 78 ff ff 	movsd  -0x88(%rbp),%xmm0
  400985:	ff 
  400986:	bf 20 10 40 00       	mov    $0x401020,%edi
  40098b:	b8 01 00 00 00       	mov    $0x1,%eax
  400990:	e8 1b fd ff ff       	callq  4006b0 <printf@plt>
  400995:	48 8b 45 98          	mov    -0x68(%rbp),%rax
  400999:	48 85 c0             	test   %rax,%rax
  40099c:	78 07                	js     4009a5 <main+0xe1>
  40099e:	f2 48 0f 2a c0       	cvtsi2sd %rax,%xmm0
  4009a3:	eb 15                	jmp    4009ba <main+0xf6>
  4009a5:	48 89 c2             	mov    %rax,%rdx
  4009a8:	48 d1 ea             	shr    %rdx
  4009ab:	83 e0 01             	and    $0x1,%eax
  4009ae:	48 09 c2             	or     %rax,%rdx
  4009b1:	f2 48 0f 2a c2       	cvtsi2sd %rdx,%xmm0
  4009b6:	f2 0f 58 c0          	addsd  %xmm0,%xmm0
  4009ba:	f2 0f 10 0d 26 07 00 	movsd  0x726(%rip),%xmm1        # 4010e8 <__dso_handle+0xd0>
  4009c1:	00 
  4009c2:	f2 0f 5e c1          	divsd  %xmm1,%xmm0
  4009c6:	be 00 e1 f5 05       	mov    $0x5f5e100,%esi
  4009cb:	bf 30 10 40 00       	mov    $0x401030,%edi
  4009d0:	b8 01 00 00 00       	mov    $0x1,%eax
  4009d5:	e8 d6 fc ff ff       	callq  4006b0 <printf@plt>
  4009da:	bf 00 08 af 2f       	mov    $0x2faf0800,%edi
  4009df:	e8 1c fd ff ff       	callq  400700 <malloc@plt>
  4009e4:	48 89 45 d0          	mov    %rax,-0x30(%rbp)
  4009e8:	48 b8 00 00 00 00 00 	movabs $0x4000000000000000,%rax
  4009ef:	00 00 40 
  4009f2:	48 8b 55 d0          	mov    -0x30(%rbp),%rdx
  4009f6:	48 89 85 78 ff ff ff 	mov    %rax,-0x88(%rbp)
  4009fd:	f2 0f 10 8d 78 ff ff 	movsd  -0x88(%rbp),%xmm1
  400a04:	ff 
  400a05:	f2 0f 10 05 d3 06 00 	movsd  0x6d3(%rip),%xmm0        # 4010e0 <__dso_handle+0xc8>
  400a0c:	00 
  400a0d:	be 00 e1 f5 05       	mov    $0x5f5e100,%esi
  400a12:	48 89 d7             	mov    %rdx,%rdi
  400a15:	e8 f6 fd ff ff       	callq  400810 <fill_double>
  400a1a:	b8 00 00 00 00       	mov    $0x0,%eax
  400a1f:	e8 ec fc ff ff       	callq  400710 <Sleef_currentTimeMicros@plt>
  400a24:	48 89 45 90          	mov    %rax,-0x70(%rbp)
  400a28:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%rbp)
  400a2f:	eb 32                	jmp    400a63 <main+0x19f>
  400a31:	48 8b 45 d0          	mov    -0x30(%rbp),%rax
  400a35:	48 8b 00             	mov    (%rax),%rax
  400a38:	48 89 85 78 ff ff ff 	mov    %rax,-0x88(%rbp)
  400a3f:	f2 0f 10 85 78 ff ff 	movsd  -0x88(%rbp),%xmm0
  400a46:	ff 
  400a47:	e8 04 03 00 00       	callq  400d50 <func2>
  400a4c:	f2 0f 10 4d d8       	movsd  -0x28(%rbp),%xmm1
  400a51:	f2 0f 58 c1          	addsd  %xmm1,%xmm0
  400a55:	f2 0f 11 45 d8       	movsd  %xmm0,-0x28(%rbp)
  400a5a:	48 83 45 d0 08       	addq   $0x8,-0x30(%rbp)
  400a5f:	83 45 e4 01          	addl   $0x1,-0x1c(%rbp)
  400a63:	81 7d e4 ff e0 f5 05 	cmpl   $0x5f5e0ff,-0x1c(%rbp)
  400a6a:	7e c5                	jle    400a31 <main+0x16d>
  400a6c:	b8 00 00 00 00       	mov    $0x0,%eax
  400a71:	e8 9a fc ff ff       	callq  400710 <Sleef_currentTimeMicros@plt>
  400a76:	48 2b 45 90          	sub    -0x70(%rbp),%rax
  400a7a:	48 89 45 90          	mov    %rax,-0x70(%rbp)
  400a7e:	48 8b 45 d8          	mov    -0x28(%rbp),%rax
  400a82:	48 89 85 78 ff ff ff 	mov    %rax,-0x88(%rbp)
  400a89:	f2 0f 10 85 78 ff ff 	movsd  -0x88(%rbp),%xmm0
  400a90:	ff 
  400a91:	bf 20 10 40 00       	mov    $0x401020,%edi
  400a96:	b8 01 00 00 00       	mov    $0x1,%eax
  400a9b:	e8 10 fc ff ff       	callq  4006b0 <printf@plt>
  400aa0:	48 8b 45 90          	mov    -0x70(%rbp),%rax
  400aa4:	48 85 c0             	test   %rax,%rax
  400aa7:	78 07                	js     400ab0 <main+0x1ec>
  400aa9:	f2 48 0f 2a c0       	cvtsi2sd %rax,%xmm0
  400aae:	eb 15                	jmp    400ac5 <main+0x201>
  400ab0:	48 89 c2             	mov    %rax,%rdx
  400ab3:	48 d1 ea             	shr    %rdx
  400ab6:	83 e0 01             	and    $0x1,%eax
  400ab9:	48 09 c2             	or     %rax,%rdx
  400abc:	f2 48 0f 2a c2       	cvtsi2sd %rdx,%xmm0
  400ac1:	f2 0f 58 c0          	addsd  %xmm0,%xmm0
  400ac5:	f2 0f 10 0d 1b 06 00 	movsd  0x61b(%rip),%xmm1        # 4010e8 <__dso_handle+0xd0>
  400acc:	00 
  400acd:	f2 0f 5e c1          	divsd  %xmm1,%xmm0
  400ad1:	be 00 e1 f5 05       	mov    $0x5f5e100,%esi
  400ad6:	bf 58 10 40 00       	mov    $0x401058,%edi
  400adb:	b8 01 00 00 00       	mov    $0x1,%eax
  400ae0:	e8 cb fb ff ff       	callq  4006b0 <printf@plt>
  400ae5:	bf 00 08 af 2f       	mov    $0x2faf0800,%edi
  400aea:	e8 11 fc ff ff       	callq  400700 <malloc@plt>
  400aef:	48 89 45 b8          	mov    %rax,-0x48(%rbp)
  400af3:	48 b8 00 00 00 00 00 	movabs $0x4000000000000000,%rax
  400afa:	00 00 40 
  400afd:	48 8b 55 b8          	mov    -0x48(%rbp),%rdx
  400b01:	48 89 85 78 ff ff ff 	mov    %rax,-0x88(%rbp)
  400b08:	f2 0f 10 8d 78 ff ff 	movsd  -0x88(%rbp),%xmm1
  400b0f:	ff 
  400b10:	f2 0f 10 05 c8 05 00 	movsd  0x5c8(%rip),%xmm0        # 4010e0 <__dso_handle+0xc8>
  400b17:	00 
  400b18:	be 00 e1 f5 05       	mov    $0x5f5e100,%esi
  400b1d:	48 89 d7             	mov    %rdx,%rdi
  400b20:	e8 eb fc ff ff       	callq  400810 <fill_double>
  400b25:	b8 00 00 00 00       	mov    $0x0,%eax
  400b2a:	e8 e1 fb ff ff       	callq  400710 <Sleef_currentTimeMicros@plt>
  400b2f:	48 89 45 88          	mov    %rax,-0x78(%rbp)
  400b33:	c7 45 cc 00 00 00 00 	movl   $0x0,-0x34(%rbp)
  400b3a:	eb 32                	jmp    400b6e <main+0x2aa>
  400b3c:	48 8b 45 b8          	mov    -0x48(%rbp),%rax
  400b40:	48 8b 00             	mov    (%rax),%rax
  400b43:	48 89 85 78 ff ff ff 	mov    %rax,-0x88(%rbp)
  400b4a:	f2 0f 10 85 78 ff ff 	movsd  -0x88(%rbp),%xmm0
  400b51:	ff 
  400b52:	e8 39 02 00 00       	callq  400d90 <func3>
  400b57:	f2 0f 10 4d c0       	movsd  -0x40(%rbp),%xmm1
  400b5c:	f2 0f 58 c1          	addsd  %xmm1,%xmm0
  400b60:	f2 0f 11 45 c0       	movsd  %xmm0,-0x40(%rbp)
  400b65:	48 83 45 b8 08       	addq   $0x8,-0x48(%rbp)
  400b6a:	83 45 cc 01          	addl   $0x1,-0x34(%rbp)
  400b6e:	81 7d cc ff e0 f5 05 	cmpl   $0x5f5e0ff,-0x34(%rbp)
  400b75:	7e c5                	jle    400b3c <main+0x278>
  400b77:	b8 00 00 00 00       	mov    $0x0,%eax
  400b7c:	e8 8f fb ff ff       	callq  400710 <Sleef_currentTimeMicros@plt>
  400b81:	48 2b 45 88          	sub    -0x78(%rbp),%rax
  400b85:	48 89 45 88          	mov    %rax,-0x78(%rbp)
  400b89:	48 8b 45 c0          	mov    -0x40(%rbp),%rax
  400b8d:	48 89 85 78 ff ff ff 	mov    %rax,-0x88(%rbp)
  400b94:	f2 0f 10 85 78 ff ff 	movsd  -0x88(%rbp),%xmm0
  400b9b:	ff 
  400b9c:	bf 20 10 40 00       	mov    $0x401020,%edi
  400ba1:	b8 01 00 00 00       	mov    $0x1,%eax
  400ba6:	e8 05 fb ff ff       	callq  4006b0 <printf@plt>
  400bab:	48 8b 45 88          	mov    -0x78(%rbp),%rax
  400baf:	48 85 c0             	test   %rax,%rax
  400bb2:	78 07                	js     400bbb <main+0x2f7>
  400bb4:	f2 48 0f 2a c0       	cvtsi2sd %rax,%xmm0
  400bb9:	eb 15                	jmp    400bd0 <main+0x30c>
  400bbb:	48 89 c2             	mov    %rax,%rdx
  400bbe:	48 d1 ea             	shr    %rdx
  400bc1:	83 e0 01             	and    $0x1,%eax
  400bc4:	48 09 c2             	or     %rax,%rdx
  400bc7:	f2 48 0f 2a c2       	cvtsi2sd %rdx,%xmm0
  400bcc:	f2 0f 58 c0          	addsd  %xmm0,%xmm0
  400bd0:	f2 0f 10 0d 10 05 00 	movsd  0x510(%rip),%xmm1        # 4010e8 <__dso_handle+0xd0>
  400bd7:	00 
  400bd8:	f2 0f 5e c1          	divsd  %xmm1,%xmm0
  400bdc:	be 00 e1 f5 05       	mov    $0x5f5e100,%esi
  400be1:	bf 80 10 40 00       	mov    $0x401080,%edi
  400be6:	b8 01 00 00 00       	mov    $0x1,%eax
  400beb:	e8 c0 fa ff ff       	callq  4006b0 <printf@plt>
  400bf0:	bf 00 08 af 2f       	mov    $0x2faf0800,%edi
  400bf5:	e8 06 fb ff ff       	callq  400700 <malloc@plt>
  400bfa:	48 89 45 a0          	mov    %rax,-0x60(%rbp)
  400bfe:	48 b8 00 00 00 00 00 	movabs $0x4000000000000000,%rax
  400c05:	00 00 40 
  400c08:	48 8b 55 a0          	mov    -0x60(%rbp),%rdx
  400c0c:	48 89 85 78 ff ff ff 	mov    %rax,-0x88(%rbp)
  400c13:	f2 0f 10 8d 78 ff ff 	movsd  -0x88(%rbp),%xmm1
  400c1a:	ff 
  400c1b:	f2 0f 10 05 bd 04 00 	movsd  0x4bd(%rip),%xmm0        # 4010e0 <__dso_handle+0xc8>
  400c22:	00 
  400c23:	be 00 e1 f5 05       	mov    $0x5f5e100,%esi
  400c28:	48 89 d7             	mov    %rdx,%rdi
  400c2b:	e8 e0 fb ff ff       	callq  400810 <fill_double>
  400c30:	b8 00 00 00 00       	mov    $0x0,%eax
  400c35:	e8 d6 fa ff ff       	callq  400710 <Sleef_currentTimeMicros@plt>
  400c3a:	48 89 45 80          	mov    %rax,-0x80(%rbp)
  400c3e:	c7 45 b4 00 00 00 00 	movl   $0x0,-0x4c(%rbp)
  400c45:	eb 32                	jmp    400c79 <main+0x3b5>
  400c47:	48 8b 45 a0          	mov    -0x60(%rbp),%rax
  400c4b:	48 8b 00             	mov    (%rax),%rax
  400c4e:	48 89 85 78 ff ff ff 	mov    %rax,-0x88(%rbp)
  400c55:	f2 0f 10 85 78 ff ff 	movsd  -0x88(%rbp),%xmm0
  400c5c:	ff 
  400c5d:	e8 5e 01 00 00       	callq  400dc0 <func4>
  400c62:	f2 0f 10 4d a8       	movsd  -0x58(%rbp),%xmm1
  400c67:	f2 0f 58 c1          	addsd  %xmm1,%xmm0
  400c6b:	f2 0f 11 45 a8       	movsd  %xmm0,-0x58(%rbp)
  400c70:	48 83 45 a0 08       	addq   $0x8,-0x60(%rbp)
  400c75:	83 45 b4 01          	addl   $0x1,-0x4c(%rbp)
  400c79:	81 7d b4 ff e0 f5 05 	cmpl   $0x5f5e0ff,-0x4c(%rbp)
  400c80:	7e c5                	jle    400c47 <main+0x383>
  400c82:	b8 00 00 00 00       	mov    $0x0,%eax
  400c87:	e8 84 fa ff ff       	callq  400710 <Sleef_currentTimeMicros@plt>
  400c8c:	48 2b 45 80          	sub    -0x80(%rbp),%rax
  400c90:	48 89 45 80          	mov    %rax,-0x80(%rbp)
  400c94:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
  400c98:	48 89 85 78 ff ff ff 	mov    %rax,-0x88(%rbp)
  400c9f:	f2 0f 10 85 78 ff ff 	movsd  -0x88(%rbp),%xmm0
  400ca6:	ff 
  400ca7:	bf 20 10 40 00       	mov    $0x401020,%edi
  400cac:	b8 01 00 00 00       	mov    $0x1,%eax
  400cb1:	e8 fa f9 ff ff       	callq  4006b0 <printf@plt>
  400cb6:	48 8b 45 80          	mov    -0x80(%rbp),%rax
  400cba:	48 85 c0             	test   %rax,%rax
  400cbd:	78 07                	js     400cc6 <main+0x402>
  400cbf:	f2 48 0f 2a c0       	cvtsi2sd %rax,%xmm0
  400cc4:	eb 15                	jmp    400cdb <main+0x417>
  400cc6:	48 89 c2             	mov    %rax,%rdx
  400cc9:	48 d1 ea             	shr    %rdx
  400ccc:	83 e0 01             	and    $0x1,%eax
  400ccf:	48 09 c2             	or     %rax,%rdx
  400cd2:	f2 48 0f 2a c2       	cvtsi2sd %rdx,%xmm0
  400cd7:	f2 0f 58 c0          	addsd  %xmm0,%xmm0
  400cdb:	f2 0f 10 0d 05 04 00 	movsd  0x405(%rip),%xmm1        # 4010e8 <__dso_handle+0xd0>
  400ce2:	00 
  400ce3:	f2 0f 5e c1          	divsd  %xmm1,%xmm0
  400ce7:	be 00 e1 f5 05       	mov    $0x5f5e100,%esi
  400cec:	bf a8 10 40 00       	mov    $0x4010a8,%edi
  400cf1:	b8 01 00 00 00       	mov    $0x1,%eax
  400cf6:	e8 b5 f9 ff ff       	callq  4006b0 <printf@plt>
  400cfb:	b8 00 00 00 00       	mov    $0x0,%eax
  400d00:	c9                   	leaveq 
  400d01:	c3                   	retq   
  400d02:	66 2e 0f 1f 84 00 00 	nopw   %cs:0x0(%rax,%rax,1)
  400d09:	00 00 00 
  400d0c:	0f 1f 40 00          	nopl   0x0(%rax)

0000000000400d10 <func1>:
  400d10:	f2 0f 10 0d f8 03 00 	movsd  0x3f8(%rip),%xmm1        # 401110 <a+0x8>
  400d17:	00 
  400d18:	f2 0f 59 c8          	mulsd  %xmm0,%xmm1
  400d1c:	f2 0f 58 0d f4 03 00 	addsd  0x3f4(%rip),%xmm1        # 401118 <a+0x10>
  400d23:	00 
  400d24:	f2 0f 59 c8          	mulsd  %xmm0,%xmm1
  400d28:	f2 0f 58 0d f0 03 00 	addsd  0x3f0(%rip),%xmm1        # 401120 <a+0x18>
  400d2f:	00 
  400d30:	f2 0f 59 c8          	mulsd  %xmm0,%xmm1
  400d34:	f2 0f 58 0d ec 03 00 	addsd  0x3ec(%rip),%xmm1        # 401128 <a+0x20>
  400d3b:	00 
  400d3c:	66 0f 28 c1          	movapd %xmm1,%xmm0
  400d40:	c3                   	retq   
  400d41:	66 66 66 66 66 66 2e 	data32 data32 data32 data32 data32 nopw %cs:0x0(%rax,%rax,1)
  400d48:	0f 1f 84 00 00 00 00 
  400d4f:	00 

0000000000400d50 <func2>:
  400d50:	66 0f 28 d0          	movapd %xmm0,%xmm2
  400d54:	66 0f 28 d8          	movapd %xmm0,%xmm3
  400d58:	f2 0f 59 d0          	mulsd  %xmm0,%xmm2
  400d5c:	66 0f 28 ca          	movapd %xmm2,%xmm1
  400d60:	f2 0f 59 da          	mulsd  %xmm2,%xmm3
  400d64:	f2 0f 59 ca          	mulsd  %xmm2,%xmm1
  400d68:	f2 0f 58 d2          	addsd  %xmm2,%xmm2
  400d6c:	f2 0f 59 1d a4 03 00 	mulsd  0x3a4(%rip),%xmm3        # 401118 <a+0x10>
  400d73:	00 
  400d74:	f2 0f 59 0d 94 03 00 	mulsd  0x394(%rip),%xmm1        # 401110 <a+0x8>
  400d7b:	00 
  400d7c:	f2 0f 58 cb          	addsd  %xmm3,%xmm1
  400d80:	f2 0f 58 ca          	addsd  %xmm2,%xmm1
  400d84:	f2 0f 58 c8          	addsd  %xmm0,%xmm1
  400d88:	66 0f 28 c1          	movapd %xmm1,%xmm0
  400d8c:	c3                   	retq   
  400d8d:	0f 1f 00             	nopl   (%rax)

0000000000400d90 <func3>:
  400d90:	f2 0f 10 15 80 03 00 	movsd  0x380(%rip),%xmm2        # 401118 <a+0x10>
  400d97:	00 
  400d98:	f2 0f 10 0d 70 03 00 	movsd  0x370(%rip),%xmm1        # 401110 <a+0x8>
  400d9f:	00 
  400da0:	c4 e2 f9 a9 ca       	vfmadd213sd %xmm2,%xmm0,%xmm1
  400da5:	f2 0f 10 15 73 03 00 	movsd  0x373(%rip),%xmm2        # 401120 <a+0x18>
  400dac:	00 
  400dad:	c4 e2 f9 a9 ca       	vfmadd213sd %xmm2,%xmm0,%xmm1
  400db2:	f2 0f 10 15 6e 03 00 	movsd  0x36e(%rip),%xmm2        # 401128 <a+0x20>
  400db9:	00 
  400dba:	c4 e2 f9 a9 ca       	vfmadd213sd %xmm2,%xmm0,%xmm1
  400dbf:	c3                   	retq   

0000000000400dc0 <func4>:
  400dc0:	55                   	push   %rbp
  400dc1:	48 89 e5             	mov    %rsp,%rbp
  400dc4:	48 83 ec 50          	sub    $0x50,%rsp
  400dc8:	c5 fb 11 04 24       	vmovsd %xmm0,(%rsp)
  400dcd:	c5 fb 10 14 25 58 11 	vmovsd 0x401158,%xmm2
  400dd4:	40 00 
  400dd6:	48 c7 c7 80 20 60 00 	mov    $0x602080,%rdi
  400ddd:	48 8d 37             	lea    (%rdi),%rsi
  400de0:	c5 fb 11 06          	vmovsd %xmm0,(%rsi)
  400de4:	c5 fb 11 47 08       	vmovsd %xmm0,0x8(%rdi)
  400de9:	c5 fb 11 47 10       	vmovsd %xmm0,0x10(%rdi)
  400dee:	c5 fb 11 57 18       	vmovsd %xmm2,0x18(%rdi)
  400df3:	c5 fd 28 0e          	vmovapd (%rsi),%ymm1
  400df7:	48 c7 c7 a0 20 60 00 	mov    $0x6020a0,%rdi
  400dfe:	48 8d 37             	lea    (%rdi),%rsi
  400e01:	c5 fb 11 06          	vmovsd %xmm0,(%rsi)
  400e05:	c5 fb 10 04 25 38 11 	vmovsd 0x401138,%xmm0
  400e0c:	40 00 
  400e0e:	c5 fb 11 47 08       	vmovsd %xmm0,0x8(%rdi)
  400e13:	c5 fb 10 04 25 48 11 	vmovsd 0x401148,%xmm0
  400e1a:	40 00 
  400e1c:	c5 fb 11 47 10       	vmovsd %xmm0,0x10(%rdi)
  400e21:	c5 fb 11 57 18       	vmovsd %xmm2,0x18(%rdi)
  400e26:	c5 fd 28 16          	vmovapd (%rsi),%ymm2
  400e2a:	c5 f5 59 ca          	vmulpd %ymm2,%ymm1,%ymm1 ########### note
  400e2e:	c5 fd 29 0c 25 80 20 	vmovapd %ymm1,0x602080
  400e35:	60 00 
  400e37:	48 c7 c7 80 20 60 00 	mov    $0x602080,%rdi
  400e3e:	c5 fb 10 07          	vmovsd (%rdi),%xmm0
  400e42:	c5 fb 11 44 24 28    	vmovsd %xmm0,0x28(%rsp)
  400e48:	c5 fb 10 47 08       	vmovsd 0x8(%rdi),%xmm0
  400e4d:	c5 fb 11 44 24 10    	vmovsd %xmm0,0x10(%rsp)
  400e53:	c5 fb 10 47 10       	vmovsd 0x10(%rdi),%xmm0
  400e58:	c5 fb 11 44 24 20    	vmovsd %xmm0,0x20(%rsp)
  400e5e:	c5 fb 10 1c 25 58 11 	vmovsd 0x401158,%xmm3
  400e65:	40 00 
  400e67:	48 c7 c7 80 20 60 00 	mov    $0x602080,%rdi
  400e6e:	48 8d 37             	lea    (%rdi),%rsi
  400e71:	c5 fb 10 44 24 28    	vmovsd 0x28(%rsp),%xmm0
  400e77:	c5 fb 11 06          	vmovsd %xmm0,(%rsi)
  400e7b:	c5 fb 11 47 08       	vmovsd %xmm0,0x8(%rdi)
  400e80:	c5 fb 11 47 10       	vmovsd %xmm0,0x10(%rdi)
  400e85:	c5 fb 11 5f 18       	vmovsd %xmm3,0x18(%rdi)
  400e8a:	c5 fd 28 0e          	vmovapd (%rsi),%ymm1
  400e8e:	48 c7 c7 a0 20 60 00 	mov    $0x6020a0,%rdi
  400e95:	48 8d 37             	lea    (%rdi),%rsi
  400e98:	c5 fb 11 06          	vmovsd %xmm0,(%rsi)
  400e9c:	c5 fb 10 44 24 10    	vmovsd 0x10(%rsp),%xmm0
  400ea2:	c5 fb 11 47 08       	vmovsd %xmm0,0x8(%rdi)
  400ea7:	c5 fb 10 04 25 40 11 	vmovsd 0x401140,%xmm0
  400eae:	40 00 
  400eb0:	c5 fb 11 47 10       	vmovsd %xmm0,0x10(%rdi)
  400eb5:	c5 fb 11 5f 18       	vmovsd %xmm3,0x18(%rdi)
  400eba:	c5 fd 28 16          	vmovapd (%rsi),%ymm2
  400ebe:	48 c7 c7 c0 20 60 00 	mov    $0x6020c0,%rdi
  400ec5:	48 8d 37             	lea    (%rdi),%rsi
  400ec8:	c5 fb 10 04 25 50 11 	vmovsd 0x401150,%xmm0
  400ecf:	40 00 
  400ed1:	c5 fb 11 06          	vmovsd %xmm0,(%rsi)
  400ed5:	c5 fb 11 47 08       	vmovsd %xmm0,0x8(%rdi)
  400eda:	c5 fb 11 47 18       	vmovsd %xmm0,0x18(%rdi)
  400edf:	c5 fb 10 44 24 20    	vmovsd 0x20(%rsp),%xmm0
  400ee5:	c5 fb 11 47 10       	vmovsd %xmm0,0x10(%rdi)
  400eea:	c5 fd 28 1e          	vmovapd (%rsi),%ymm3
  400eee:	c4 e2 ed a8 cb       	vfmadd213pd %ymm3,%ymm2,%ymm1 ############### note
  400ef3:	c5 fd 29 0c 25 80 20 	vmovapd %ymm1,0x602080
  400efa:	60 00 
  400efc:	48 c7 c7 80 20 60 00 	mov    $0x602080,%rdi
  400f03:	c5 fb 10 07          	vmovsd (%rdi),%xmm0
  400f07:	c5 fb 11 44 24 30    	vmovsd %xmm0,0x30(%rsp)
  400f0d:	c5 fb 10 47 08       	vmovsd 0x8(%rdi),%xmm0
  400f12:	c5 fb 11 44 24 38    	vmovsd %xmm0,0x38(%rsp)
  400f18:	c5 fb 10 47 10       	vmovsd 0x10(%rdi),%xmm0
  400f1d:	c5 fb 11 44 24 40    	vmovsd %xmm0,0x40(%rsp)
  400f23:	48 c7 c7 80 20 60 00 	mov    $0x602080,%rdi
  400f2a:	c5 fb 10 04 25 30 11 	vmovsd 0x401130,%xmm0
  400f31:	40 00 
  400f33:	c5 fb 11 07          	vmovsd %xmm0,(%rdi)
  400f37:	c5 fb 10 1c 25 58 11 	vmovsd 0x401158,%xmm3
  400f3e:	40 00 
  400f40:	c5 fb 11 5f 08       	vmovsd %xmm3,0x8(%rdi)
  400f45:	c5 fb 11 5f 10       	vmovsd %xmm3,0x10(%rdi)
  400f4a:	c5 fb 11 5f 18       	vmovsd %xmm3,0x18(%rdi)
  400f4f:	c5 fd 28 0f          	vmovapd (%rdi),%ymm1
  400f53:	c5 fd 28 d1          	vmovapd %ymm1,%ymm2
  400f57:	c5 fd 28 d9          	vmovapd %ymm1,%ymm3
  400f5b:	c5 fb 10 54 24 30    	vmovsd 0x30(%rsp),%xmm2
  400f61:	c5 fb 10 5c 24 38    	vmovsd 0x38(%rsp),%xmm3
  400f67:	c4 e2 ed a8 cb       	vfmadd213pd %ymm3,%ymm2,%ymm1 ############## note
  400f6c:	c5 fd 29 0c 25 80 20 	vmovapd %ymm1,0x602080
  400f73:	60 00 
  400f75:	48 c7 c7 80 20 60 00 	mov    $0x602080,%rdi
  400f7c:	c5 fb 10 07          	vmovsd (%rdi),%xmm0
  400f80:	c5 fb 58 44 24 40    	vaddsd 0x40(%rsp),%xmm0,%xmm0
  400f86:	48 83 c4 50          	add    $0x50,%rsp
  400f8a:	48 89 ec             	mov    %rbp,%rsp
  400f8d:	5d                   	pop    %rbp
  400f8e:	c3                   	retq   
  400f8f:	90                   	nop

0000000000400f90 <__libc_csu_init>:
  400f90:	41 57                	push   %r15
  400f92:	41 89 ff             	mov    %edi,%r15d
  400f95:	41 56                	push   %r14
  400f97:	49 89 f6             	mov    %rsi,%r14
  400f9a:	41 55                	push   %r13
  400f9c:	49 89 d5             	mov    %rdx,%r13
  400f9f:	41 54                	push   %r12
  400fa1:	4c 8d 25 58 0e 20 00 	lea    0x200e58(%rip),%r12        # 601e00 <__frame_dummy_init_array_entry>
  400fa8:	55                   	push   %rbp
  400fa9:	48 8d 2d 58 0e 20 00 	lea    0x200e58(%rip),%rbp        # 601e08 <__init_array_end>
  400fb0:	53                   	push   %rbx
  400fb1:	4c 29 e5             	sub    %r12,%rbp
  400fb4:	31 db                	xor    %ebx,%ebx
  400fb6:	48 c1 fd 03          	sar    $0x3,%rbp
  400fba:	48 83 ec 08          	sub    $0x8,%rsp
  400fbe:	e8 ad f6 ff ff       	callq  400670 <_init>
  400fc3:	48 85 ed             	test   %rbp,%rbp
  400fc6:	74 1e                	je     400fe6 <__libc_csu_init+0x56>
  400fc8:	0f 1f 84 00 00 00 00 	nopl   0x0(%rax,%rax,1)
  400fcf:	00 
  400fd0:	4c 89 ea             	mov    %r13,%rdx
  400fd3:	4c 89 f6             	mov    %r14,%rsi
  400fd6:	44 89 ff             	mov    %r15d,%edi
  400fd9:	41 ff 14 dc          	callq  *(%r12,%rbx,8)
  400fdd:	48 83 c3 01          	add    $0x1,%rbx
  400fe1:	48 39 eb             	cmp    %rbp,%rbx
  400fe4:	75 ea                	jne    400fd0 <__libc_csu_init+0x40>
  400fe6:	48 83 c4 08          	add    $0x8,%rsp
  400fea:	5b                   	pop    %rbx
  400feb:	5d                   	pop    %rbp
  400fec:	41 5c                	pop    %r12
  400fee:	41 5d                	pop    %r13
  400ff0:	41 5e                	pop    %r14
  400ff2:	41 5f                	pop    %r15
  400ff4:	c3                   	retq   
  400ff5:	66 66 2e 0f 1f 84 00 	data32 nopw %cs:0x0(%rax,%rax,1)
  400ffc:	00 00 00 00 

0000000000401000 <__libc_csu_fini>:
  401000:	f3 c3                	repz retq 
  401002:	66 90                	xchg   %ax,%ax

Disassembly of section .fini:

0000000000401004 <_fini>:
  401004:	48 83 ec 08          	sub    $0x8,%rsp
  401008:	48 83 c4 08          	add    $0x8,%rsp
  40100c:	c3                   	retq   
