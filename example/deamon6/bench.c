#include <stdio.h>
#include <stdlib.h>
#include "sleef.h"
#include <time.h>
double func1(double x);
double func2(double x);
double func3(double x);
double func4(double x);
void fill_double(double *buf,int times ,double min, double max) {
  srandom(time(NULL));
  int i = 0;
  for(i=0;i<times;i++) {
    double r = ((double)random() + RAND_MAX * (double)random()) / (RAND_MAX * (double)RAND_MAX);
    buf[i] = r * (max - min) + min;
  }
}

#define bench_func(funcname,op_min,op_max,times) ({\
    int i;\
    uint64_t t;\
    double acc;\
    double *buf = malloc(times*8);\
    fill_double(buf,times,op_min,op_max);\
    t = Sleef_currentTimeMicros(); \
    for(i = 0 ; i < times;i++){\
        acc += funcname(*buf);\
        buf++;\
    }\
    t = Sleef_currentTimeMicros() - t;\
    printf("acc : %lf\n",acc);\
    printf("funcname : "#funcname",init : %d,time : %lf\n",times,(double)t/(times));\
})

#define INT 100000000

int main(){
    bench_func(func1,1.0,2.0,INT);
    bench_func(func2,1.0,2.0,INT);
    bench_func(func3,1.0,2.0,INT);
    bench_func(func4,1.0,2.0,INT);
    return 0;
}
