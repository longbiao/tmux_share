#include <stdio.h>
const double a = 5.0;
const double b = 3.0;
const double c = 2.0;
const double d = 1.0;
#define INLINE inline

static INLINE double fma_s(double a,double b,double c){
    //VFMADD132SD
    __asm__ ("vfmadd213sd %3,%2,%1":"=x"(a):"x"(a),"x"(b),"x"(c)); //
    return a;
}
double func1(double x){
// x(x(x(ax+b)+c)+d)
    double tmp;
    tmp = ((a*x+b)*x + c ) * x + d;
    return tmp;
}

double func2(double x){
//a*x^4 + b*x^3+c*x^2+d*x
    double x2 = x * x;
    double x3 = x * x2;
    double x4 = x2 * x2;
    double tmp;
    tmp = a * x4 + b * x3 + c * x2 + d * x;
    return tmp;
}

double func3(double x){
//fmad version of func1
    double tmp;
    tmp = fma_s(fma_s(fma_s(a,x,b),x,c),x,d);
    return tmp;
}
// double func4(double x);
// vector version of func2
// implement in func4.s