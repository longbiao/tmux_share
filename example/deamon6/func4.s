/*this is the function :
    return a * x^4 + b * x^3 + c * x^2 + d * x
*/
.section .bss
.align 32      # this is important
.lcomm vda,32  # vector vda
.lcomm vdb,32  # vector vdb
.lcomm vdc,32  # vector vdc
.lcomm vdd,32  # vector vdd

/*the area of var*/
.equ ox , 0  # x
.equ ax , 8  # a*x
.equ bx , 16 # b*x
.equ cx , 24 # c*x
.equ dx , 32 # d*x
.equ x2 , 40 # x^2
.equ x4 , 48 # x^4
.equ bx3, 56 # b*x^3
.equ cx2_dx, 64 # c*x^2 + dx
.equ resu, 72   # return result

.section .rodata, "a"
A:
    .long   0x00000000,0x40140000 # 5.0
B:
    .long   0x00000000,0x40080000 # 3.0
C:
    .long   0x00000000,0x40000000 # 2.0
D:
    .long   0x00000000,0x3ff00000 # 1.0
ZERO:
    .long   0x00000000,0x00000000 # 0.0
ONE:
    .long   0x00000000,0x3ff00000 # 1.0


.section .text
.globl func4
.type func4,@function

func4:
    push %rbp
    movq  %rsp, %rbp
    sub  $0x50,%rsp
    vmovsd %xmm0,ox(%rsp) # store x in ox

    vmovsd ONE,%xmm2
    movq $vda,%rdi
    leaq 0(%rdi),%rsi
    vmovsd %xmm0,(%rsi) # vda[0] = x 
    vmovsd %xmm0,8(%rdi) # vda[1] = x
    vmovsd %xmm0,16(%rdi) # vda[2] = x
    vmovsd %xmm2,24(%rdi)  # vda[3] = 1
    vmovapd (%rsi),%ymm1 # const vda
    movq $vdb,%rdi
    leaq 0(%rdi),%rsi
    vmovsd %xmm0,(%rsi) # vdb[0] = x
    vmovsd B,%xmm0
    vmovsd %xmm0,8(%rdi) # vdb[1] = B
    vmovsd D,%xmm0
    vmovsd %xmm0,16(%rdi) # vdb[2] = D
    vmovsd %xmm2,24(%rdi)  # vdb[3] = 1
    vmovapd (%rsi),%ymm2 # const vdb
    vmulpd %ymm2,%ymm1,%ymm1                     # ###########################
    vmovapd %ymm1,vda  # vda = vda * vdb

    movq $vda,%rdi
    vmovsd 0(%rdi),%xmm0
    vmovsd %xmm0,x2(%rsp) # store x^2 in x2
    vmovsd 8(%rdi),%xmm0
    vmovsd %xmm0,bx(%rsp) # store b*x in bx
    vmovsd 16(%rdi),%xmm0
    vmovsd %xmm0,dx(%rsp) # store d*x in cx


    vmovsd ONE,%xmm3
    movq $vda,%rdi
    leaq 0(%rdi),%rsi
    vmovsd x2(%rsp),%xmm0
    vmovsd %xmm0,(%rsi)   # vda[0] = x^2
    vmovsd %xmm0,8(%rdi)  # vda[1] = x^2
    vmovsd %xmm0,16(%rdi) # vda[2] = x^2
    vmovsd %xmm3,24(%rdi) # vda[3] = 1.0
    vmovapd (%rsi),%ymm1   # const vda
    movq $vdb,%rdi
    leaq 0(%rdi),%rsi
    vmovsd %xmm0,(%rsi)   # vdb[0] = x^2
    vmovsd bx(%rsp),%xmm0 # note : is bx(%rsp),not bs
    vmovsd %xmm0,8(%rdi)  # vdb[1] = bx
    vmovsd C,%xmm0
    vmovsd %xmm0,16(%rdi) # vdb[2] = c
    vmovsd %xmm3,24(%rdi) # vdb[4] = 1.0
    vmovapd (%rsi),%ymm2  # const vdb
    movq $vdc,%rdi
    leaq 0(%rdi),%rsi
    vmovsd ZERO,%xmm0
    vmovsd %xmm0,(%rsi)   # vdc[0] = 0.0
    vmovsd %xmm0,8(%rdi)  # vdc[1] = 0.0
    vmovsd %xmm0,24(%rdi) # vdc[3] = 0.0
    vmovsd dx(%rsp),%xmm0       
    vmovsd %xmm0,16(%rdi) # vdc[2] = dx
    vmovapd (%rsi),%ymm3  # const vdc
    vfmadd213pd %ymm3,%ymm2,%ymm1
    vmovapd %ymm1,vda     # vda = vda*vdb + vdc

    movq $vda,%rdi
    vmovsd (%rdi),%xmm0
    vmovsd %xmm0,x4(%rsp)  # store x^4 in x4
    vmovsd 8(%rdi),%xmm0
    vmovsd %xmm0,bx3(%rsp) # store b*x^3 in bx3
    vmovsd 16(%rdi),%xmm0
    vmovsd %xmm0,cx2_dx(%rsp) # store c*x2+dx  in cx2_dx


    movq $vda,%rdi
    vmovsd A,%xmm0
    vmovsd %xmm0,0(%rdi)   # vda[0] = a
    vmovsd ONE,%xmm3
    vmovsd %xmm3,8(%rdi)   # vda[1] = 1.0
    vmovsd %xmm3,16(%rdi)  # vda[2] = 1.0
    vmovsd %xmm3,24(%rdi)  # vda[3] = 1.0
    vmovapd (%rdi),%ymm1   #const vda
    vmovapd %ymm1,%ymm2    #const vdb
    vmovapd %ymm1,%ymm3    #const vdc
    vmovsd x4(%rsp),%xmm2  #vdb[0] = x^4
    vmovsd bx3(%rsp),%xmm3 #vdc[0] = b*x^3
    vfmadd213pd %ymm3,%ymm2,%ymm1             
    vmovapd %ymm1,vda      # vda = vda*vdb + vdc

    movq $vda,%rdi
    vmovsd (%rdi),%xmm0    # a*x^4+b*x^3 
    vaddsd cx2_dx(%rsp),%xmm0,%xmm0 # a*x^4+b*x^3 + c*x^2 + d * x   

    add $0x50,%rsp
    movq %rbp,%rsp
    pop %rbp
    ret
    