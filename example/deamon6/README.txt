该目录是是一种可能性测试:
    对如下形式的多项展开式:a * x^4 + b * x^3 + c * x^2 + d * x
    是否能对齐进行可能的并行化(向量化以及乘加计算)后性能能否提升,而不是使用多数libm代码中的((a*x+b)*x + c ) * x + d的形式(当然这两者的结果会是截然不同)

文件解释:
    f2w  f2w.c
        浮点数与其位模式互相转换的tool程序
    test* 系列
        学习程序
    bench.c
        主体测试程序,使用了sleef的计时函数,需要sleef.h 和 libsleef支持
    func.c func4.s
        多项式展开的四种使用形式
    Makefile
        make bench 即可编译bench

